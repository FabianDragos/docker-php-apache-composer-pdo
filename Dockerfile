FROM php:7.3-apache

RUN ssh-keyscan gitlab.com > ~/.ssh/known_hosts; ssh-keyscan github.com >> ~/.ssh/known_hosts; mkdir -p /var/www/sms-service
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN a2enmod rewrite
RUN apt-get update && \
    apt-get install -y git unzip zlib1g-dev libzip-dev libpng-dev && \
    apt-get install xvfb libfontconfig wkhtmltopdf -y && \
    echo "Clean apt junk" && \
    rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install pdo pdo_mysql zip gd mbstring
WORKDIR /var/www/